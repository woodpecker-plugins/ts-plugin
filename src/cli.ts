export type SettingType = 'string' | 'number' | 'boolean' | 'json';
export type SettingTypeValue<T extends SettingType> = T extends 'boolean'
  ? boolean
  : T extends 'string'
    ? string
    : T extends 'number'
      ? number
      : T extends 'json'
        ? unknown
        : never;
export type SettingDef<T extends SettingType> = {
  type: T;
  description: string;
  required?: boolean;
  default?: SettingTypeValue<T>;
};

export type BooleanArgDef = SettingDef<'boolean'>;
export type StringArgDef = SettingDef<'string'>;
export type NumberArgDef = SettingDef<'number'>;
export type JSONArgDef = SettingDef<'json'>;
export type SettingsDef = Record<string, BooleanArgDef | StringArgDef | NumberArgDef | JSONArgDef>;

export type ParsedSettings<T extends SettingsDef> = {
  [K in keyof T]: T[K]['required'] extends true
    ? SettingTypeValue<T[K]['type']>
    : SettingTypeValue<T[K]['type']> | undefined;
};

export type MetaDef = {
  name: string;
  version?: string;
  description?: string;
};

export type CommandDef<T extends SettingsDef> = {
  meta: MetaDef;
  settings: T;
  run: (ctx: { settings: ParsedSettings<T> }) => void | Promise<void>;
};

export function runPlugin<T extends SettingsDef>(options: CommandDef<T>) {
  const settingsValues = Object.fromEntries(
    Object.entries(options.settings).map(([name, setting]) => {
      const value = process.env[`PLUGIN_${name.toUpperCase()}`];
      if (setting.required && value === undefined) {
        throw new Error(`Missing required setting: ${name}`);
      }

      if (value === undefined) {
        return undefined;
      }

      if (setting.type === 'string') {
        return value;
      }

      if (setting.type === 'number') {
        const number = Number(value);
        if (isNaN(number)) {
          throw new Error(`Invalid number setting: ${name}`);
        }
        return number;
      }

      if (setting.type === 'boolean') {
        if (value.toLocaleLowerCase() === 'true') {
          return true;
        }

        if (value.toLocaleLowerCase() === 'false') {
          return false;
        }

        throw new Error(`Invalid boolean setting: ${name}`);
      }

      if (setting.type === 'json') {
        try {
          return JSON.parse(value);
        } catch (error) {
          throw new Error(`Invalid JSON setting: ${name}`);
        }
      }
    }),
  ) as ParsedSettings<T>;

  options.run({ settings: settingsValues });
}
