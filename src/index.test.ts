import { runPlugin } from './index';

runPlugin({
  meta: {
    name: 'hello',
    version: '1.0.0',
    description: 'My Awesome CLI App',
  },
  settings: {
    name: {
      type: 'string',
      description: 'Your name',
      required: true,
    },
    friendly: {
      type: 'boolean',
      description: 'Use friendly greeting',
    },
  },
  async run({ settings }) {
    console.log(`${settings.friendly ? 'Hi' : 'Greetings'} ${settings.name}!`);
  },
});
