export type SettingsOptions = { required?: boolean; trimWhiteSpace?: boolean };

export function getSetting(key: string, options?: SettingsOptions): string | undefined {
  const setting = process.env[`PLUGIN_${key.replace(/ /g, '_').toUpperCase()}`];
  if (options?.required && setting === undefined) {
    throw new Error(`Missing required setting: ${key}`);
  }

  if (options?.trimWhiteSpace) {
    return setting?.trim();
  }

  return setting;
}

export function getNumberSetting(key: string, options: SettingsOptions & { required: true }): number;
export function getNumberSetting(key: string, options: SettingsOptions): number | undefined;
export function getNumberSetting(key: string, options?: SettingsOptions): number | undefined {
  const setting = getSetting(key, options);
  if (setting === undefined) {
    return undefined;
  }

  const number = Number(setting);
  if (isNaN(number)) {
    throw new Error(`Invalid number setting: ${key}`);
  }

  return number;
}

export function getBooleanSetting(key: string, options: SettingsOptions & { required: true }): boolean;
export function getBooleanSetting(key: string, options: SettingsOptions): boolean | undefined;
export function getBooleanSetting(key: string, options?: SettingsOptions): boolean | undefined {
  const setting = getSetting(key, options);
  if (setting === undefined) {
    return undefined;
  }

  if (setting.toLocaleLowerCase() === 'true') {
    return true;
  }

  if (setting.toLocaleLowerCase() === 'false') {
    return false;
  }

  throw new Error(`Invalid boolean setting: ${key}`);
}

export function getJSONSetting<T = unknown>(key: string, options: SettingsOptions & { required: true }): T;
export function getJSONSetting<T = unknown>(key: string, options: SettingsOptions): T | undefined;
export function getJSONSetting<T = unknown>(key: string, options?: SettingsOptions): T | undefined {
  const setting = getSetting(key, options);
  if (setting === undefined) {
    return undefined;
  }

  try {
    return JSON.parse(setting);
  } catch (e) {
    throw new Error(`Invalid JSON setting: ${key}`);
  }
}
