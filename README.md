# ts-plugin

Utilities to create a [Woodpecker-CI](https://woodpecker-ci.org) typescript / javascript plugin.

## Creating plugin

```typescript
import { runPlugin } from '@woodpecker-ci/plugin';

runPlugin({
  meta: {
    name: 'my-plugin',
    version: '1.0.0',
    description: 'My plugin description',
  },
  settings: {
    name: {
      type: 'string',
      description: 'Your name',
      required: true,
    },
    friendly: {
      type: 'boolean',
      description: 'Use friendly greeting',
    },
  },
  async run({ settings }) {
    console.log(`${settings.friendly ? 'Hi' : 'Greetings'} ${settings.name}!`);
  },
});
```

### Use it in Woodpecker-CI

```yaml
steps:
  - image: your-image
    name: my-plugin
    settings:
      name: John
      friendly: true
```
